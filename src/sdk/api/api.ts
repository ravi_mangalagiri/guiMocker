export * from './orders.service';
import { OrdersService } from './orders.service';
export * from './orders.serviceInterface';
export * from './system.service';
import { SystemService } from './system.service';
export * from './system.serviceInterface';
export const APIS = [OrdersService, SystemService];
